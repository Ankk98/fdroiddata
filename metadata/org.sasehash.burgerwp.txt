Categories:Theming
License:GPL-3.0-only
Web Site:
Source Code:https://github.com/samsumas/LivingBurger
Issue Tracker:https://github.com/samsumas/LivingBurger/issues

Auto Name:BurgerWP
Summary:Get rid of your old boring Wallpaper and replace it with burgers and pizza
Description:
Very nice Wallpaper featuring some pizza and burger. Burger bounces around and
the pizzas slide from one side to another. Fork me on GitHub!
.

Repo Type:git
Repo:https://github.com/samsumas/LivingBurger.git

Build:1.0,1
    commit=v1.0
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0
Current Version Code:1
